//Cédula, apellidos, nombres, dirección, semestre, paralelo, correo electrónico

//Validar los datos//
const expresiones = {
    cedula: /^\d{1,10}$/,
    apellidos: /^[a-zA-ZÀ-ÿ\s\ ]{5,100}$/,
    nombres: /^[a-zA-ZÀ-ÿ\s\ ]{5,100}$/,
    direccion: /^[a-zA-Z0-9\,\.\ \-\-]{4,200}$/,
    semestre: /^[a-zA-Z0-9]{5,50}$/,
    paralelo: /^[a-zA-Z0-9]{5,50}$/,
    correo: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/
}


//Funcion para registro del estudiante
const registrarEstudiante = async (req, res) => {
    try {
        const {
            cedula,
            apellidos,
            nombres,
            direccion,
            semestre,
            paralelo,
            correo
        } = req.body
        const EstudianteIngresado = await pool.query(
            "insert into Estudiantes(ced_est, ape_est, nom_est, dir_est, sem_est, par_est, mail_est) values($1, $2, $3, $4, $5, $6, $7) returning *",
            [cedula, apellidos, nombres, direccion, semestre, paralelo, correo], (error, resultado) => {
                if (error) res.json(error)
                else res.json(resultado.rows)
            });

    } catch (error) {
        return (error);
    }
}